**REMEMBER! Add ~"discussion" tag to this issue to mark it as a discussion!**

**Who's here**
_@mention attendees to send to their To Do so they can review when you're done!)_

- Convener: ___
- Facilitator: ___
- Note Taker: ___
- Others: 

**Outcomes** _List New Tasks / Milestones / Epics created from this meeting_
- Issues 1 (w/ link to gitlab issue)

**Agenda** _set this ahead of time! invited folks can add/adjust_

**Meeting Eval** _What was good, what could be improved?_

**Discussion** _notes from the actual discussion_

Refresher: [How we do project management link](https://gitlab.com/groups/our-sci/-/wikis/Getting-Started)
